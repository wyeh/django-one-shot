from django.urls import path
from todos.views import (
TodoListView, 
TodoDetailView, 
TodoCreateView,
TodoDeleteView,
TodoUpdateView,
TodoItemCreateView,
TodoItemUpdateView,
)


urlpatterns = [
    path("", TodoListView.as_view(), name='todos_list'),
    path("<int:pk>/", TodoDetailView.as_view(), name='todos_detail'),
    path("create/", TodoCreateView.as_view(), name='todos_create'),
    path("<int:pk>/edit/", TodoUpdateView.as_view(), name='todos_edit'),
    path("<int:pk>/delete/", TodoDeleteView.as_view(), name='todos_delete'),
    path("items/create/", TodoItemCreateView.as_view(), name='item_create'),
    path("items/<int:pk/edit/", TodoItemUpdateView.as_view(), name='item_edit'),
]
