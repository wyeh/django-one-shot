from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from todos.models import TodoList, TodoItem

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = 'todos/list.html'

class TodoDetailView(DetailView):
    model = TodoList
    template_name = 'todos/detail.html'

class TodoCreateView(CreateView):
    model = TodoList
    template_name = 'todos/create.html'
    fields = ['name']

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = 'todos/edit.html'
    fields = ['name']
    success_url = reverse_lazy('todos_list')

class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = 'todos/delete.html'
    success_url = reverse_lazy('todos_list')

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = 'todos/items/edit.html'
    fields = ['task', 'due_date']
    succes_url = reverse_lazy('item_create')

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = 'todos/items/create.html'
    fields =['task', 'due_date']
    success_url = reverse_lazy('todos_list')

